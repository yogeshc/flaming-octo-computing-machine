//============================================================================
// Name        : cpp_examples.cpp
// Author      : Yogesh Chaudhari
// Version     :
// Copyright   : All Code (c)2013 Yogesh Chaudhari All rights reserved
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	return 0;
}
